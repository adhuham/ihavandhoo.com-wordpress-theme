<?php

define('THISTHEME_NAME', 'Ihavandhoo Online');
define('THISTHEME_SLUG', 'ihavandhoo_online');


/**
 * Theme Setiup
*/
function thistheme_init() {

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );

	register_nav_menus( array(
		'primary' => esc_html__( 'Main Menu', 'thistheme' ),
	) );

    add_theme_support( 'post-thumbnails' );
    add_image_size( 'thistheme-thumb', 450, 270, true );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

}
add_action( 'after_setup_theme', 'thistheme_init' );


/**
 * Enqueues scripts and styles.
 */
function thistheme_scripts() {

	wp_enqueue_style( THISTHEME_SLUG.'-tailwind', 	get_template_directory_uri() . '/css/tailwind.css', array(), '' );
	wp_enqueue_style( THISTHEME_SLUG.'-bootstrap-grid', 	get_template_directory_uri() . '/css/bootstrap-grid.rtl.min.css', array(), '' );
	wp_enqueue_style( THISTHEME_SLUG.'-fontawesome', 	get_template_directory_uri() . '/css/font-awesome.min.css', array(), '' );
	wp_enqueue_style( THISTHEME_SLUG.'-app', 	get_template_directory_uri() . '/css/app.css', array(), '5' );
 	wp_enqueue_style( THISTHEME_SLUG.'-style', 	get_stylesheet_uri(), array(), '' );

 	wp_enqueue_script('jquery-3.5.1', 	get_template_directory_uri() . '/js/jquery-3.5.1.min.js', array(), '', true );
    wp_enqueue_script('thaana', get_template_directory_uri() . '/js/jquery.thaana.min.js', array(), '', true );
    wp_enqueue_script('app', get_template_directory_uri() . '/js/app.js', array(), '', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'thistheme_scripts' );

function thistheme_title( $title ) {
	if (is_single() || is_page()) {
        $latin = get_post_meta(get_the_ID(), 'custom-latin-heading', true);
        return $latin  . ' - ' . get_bloginfo('name');
    }

    return $title;
}
apply_filters( 'document_title_separator', '-' );
add_filter( 'pre_get_document_title', 'thistheme_title' );


/**
 * Social meta tags.
 */
function thistheme_social_meta()
{
	// $logo = wp_get_attachment_image_src(get_theme_mod('custom_logo') , 'full');
	// $logo_img = has_custom_logo() ? esc_url( $logo[0] ) : get_template_directory_uri().'/assets/favicon.png';

	$logo_img = get_template_directory_uri() . '/assets/og.png';

	if(is_home()):
?>
		<meta name="description" content="<?php echo esc_html(get_bloginfo('description')); ?>">
		<meta name="twitter:card" value="summary">

		<meta property="og:title" content="<?php echo esc_html(get_bloginfo('name')); ?>">
		<meta property="og:type" content="article">
		<meta property="og:url" content="<?php echo esc_url(get_bloginfo('url')); ?>">
		<meta property="og:image" content="<?php echo $logo_img; ?>">
		<meta property="og:description" content="<?php echo esc_html(get_bloginfo('description')); ?>">

		<meta name="twitter:title" content="<?php echo esc_html(get_bloginfo('name')); ?>">
		<meta name="twitter:description" content="<?php echo esc_html(get_bloginfo('description')); ?>">
		<meta name="twitter:image" content="<?php echo $logo_img; ?>">

<?php
	elseif(is_single() || is_page()):
        $title = get_post_meta(get_the_ID(), 'custom-latin-heading', true);
		$desc = has_excerpt() ? get_the_excerpt() : get_bloginfo('description');

		$img = has_post_thumbnail() ?
                get_the_post_thumbnail_url( get_the_ID ( ) , 'full' ) : 
                $logo_img;
	?>
		<meta name="description" content="<?php echo esc_html($desc); ?>">
		<meta name="twitter:card" value="summary">

		<meta property="og:title" content="<?php echo esc_html(ucwords($title)); ?>">
		<meta property="og:type" content="article">
		<meta property="og:url" content="<?php echo esc_url(get_permalink()); ?>">
		<meta property="og:image" content="<?php echo $img; ?>">
		<meta property="og:description" content="<?php echo esc_html($desc); ?>">

		<meta name="twitter:title" content="<?php echo esc_html(ucwords($title)); ?>">
		<meta name="twitter:description" content="<?php echo esc_html($desc); ?>">
		<meta name="twitter:image" content="<?php echo $img; ?>">
<?php
	elseif(is_archive()):
?>
	<meta name="description" content="<?php echo esc_html(get_bloginfo('description')); ?>">
	<meta name="twitter:card" value="summary">

	<meta property="og:title" content="<?php echo esc_html(get_the_archive_title()); ?>">
	<meta property="og:type" content="article">
	<meta property="og:url" content="<?php echo esc_url($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); ?>">
	<meta property="og:image" content="<?php echo $logo_img; ?>">
	<meta property="og:description" content="<?php echo esc_html(get_bloginfo('description')); ?>">

	<meta name="twitter:title" content="<?php echo esc_html(get_the_archive_title()); ?>">
	<meta name="twitter:description" content="<?php echo esc_html(get_bloginfo('description')); ?>">
	<meta name="twitter:image" content="<?php echo $logo_img; ?>">

<?php
	endif;
}
add_theme_support( 'post-thumbnails' );


/**
 * Comment custom feedback/success message
 */
add_action( 'set_comment_cookies', function( $comment, $user ) {
    setcookie( 'ta_comment_wait_approval', '1', 0, '/' );
}, 10, 2 );

add_action( 'init', function() {
    if( isset( $_COOKIE['ta_comment_wait_approval'] ) && $_COOKIE['ta_comment_wait_approval'] === '1' ) {
        setcookie( 'ta_comment_wait_approval', '0', 0, '/' );
        add_action( 'comment_form_after', function() {
            echo '<p id="wait_approval" class="ltr mt-3 text-green-800 text-right">Your comment has been sent successfully.</p>';
        });
    }
});

add_filter( 'comment_post_redirect', function( $location, $comment ) {
    $location = get_permalink( $comment->comment_post_ID ) . '#wait_approval';
    return $location;
}, 10, 2 );


/**
 * Custom excerpt length.
 */
function thistheme_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'thistheme_excerpt_length', 999 );



/**
 * Excerpt more text.
 */
function thistheme_excerpt_more( $more ) {
    return '';
}
add_filter('excerpt_more', 'thistheme_excerpt_more');


remove_action( 'welcome_panel', 'wp_welcome_panel' );

function thistheme_disable_dashboard_widgets() {
    remove_meta_box('dashboard_right_now', 'dashboard', 'normal');// Remove "At a Glance"
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');// Remove Quick Draft
    remove_meta_box('dashboard_primary', 'dashboard', 'core');// Remove WordPress Events and News
}
add_action('admin_menu', 'thistheme_disable_dashboard_widgets');


function thistheme_hash_filename( $filename ) {
	date_default_timezone_set('Asia/Karachi');

	$info = pathinfo( $filename );
	$ext  = empty( $info['extension'] ) ? '' : '.' . $info['extension'];
	$name = date('YmdHis') . '_' . time();

	return $name . $ext;
}

add_filter( 'sanitize_file_name', 'thistheme_hash_filename', 10 );


/**
 * Custom taxonomies
 */
function wporg_register_taxonomy_course() {
     $labels = array(
         'name'              => _x( 'Kickers', 'taxonomy general name' ),
         'singular_name'     => _x( 'Kicker', 'taxonomy singular name' ),
         'search_items'      => __( 'Search Kickers' ),
         'all_items'         => __( 'All Kickers' ),
         'parent_item'       => __( 'Parent Kicker' ),
         'parent_item_colon' => __( 'Parent Kicker:' ),
         'edit_item'         => __( 'Edit Kicker' ),
         'update_item'       => __( 'Update Kicker' ),
         'add_new_item'      => __( 'Add New Kicker' ),
         'new_item_name'     => __( 'New Kicker Name' ),
         'menu_name'         => __( 'Kicker' ),
     );
     $args   = array(
         'hierarchical'      => false, // make it hierarchical (like categories)
         'labels'            => $labels,
         'show_ui'           => true,
         'show_admin_column' => false,
         'query_var'         => true,
         'rewrite'           => [ 'slug' => 'kicker' ],
     );
     register_taxonomy( 'kicker', [ 'post' ], $args );
}
add_action( 'init', 'wporg_register_taxonomy_course' );


/**
 * Custom meta boxes
 */
function thistheme_register_meta_boxes( $meta_boxes ) {
    $prefix = 'custom-';

    $meta_boxes[] = [
        'title'      => esc_html__( 'Meta', 'online-generator' ),
        'post_types' => ['post'],
        'context'    => 'after_title',
        'priority'   => 'high',
        'autosave'   => true,
        'fields'     => [
            [
                'type' => 'heading',
                'name' => esc_html__( 'Kicker', 'online-generator' ),
                'desc' => esc_html__( '', 'online-generator' ),
            ],
            [
                'type'  => 'text',
                'class' => 'thaana',
                'id'    => $prefix . 'kicker',
                'name'  => esc_html__( 'Kicker', 'online-generator' ),
            ],
            [
                'type' => 'heading',
                'name' => esc_html__( 'Short Heading', 'online-generator' ),
                'desc' => esc_html__( 'Write a shorter version of main article heading. Max 80 characters.', 'online-generator' ),
            ],
            [
                'type'  => 'text',
                'id'    => $prefix . 'short-heading',
                'size'  => 80,
                'name'  => esc_html__( 'Short Heading', 'online-generator' ),
                'class' => 'thaana',
            ],
            [
                'type' => 'heading',
                'name' => esc_html__( 'Latin Heading', 'online-generator' ),
                'desc' => esc_html__( 'Latin heading for social media.', 'online-generator' ),
            ],
            [
                'type' => 'text',
                'id'   => $prefix . 'latin-heading',
                'name' => esc_html__( 'Latin Heading', 'online-generator' ),
            ],
        ],
	'validation' => [
		'rules' => [
			$prefix . 'short-heading' => [
				'required' => true,
				'maxlength' => 80
			]
		]
	]
    ];

    return $meta_boxes;
}

add_filter( 'rwmb_meta_boxes', 'thistheme_register_meta_boxes' );


/**
 * Admin section fixes and changes
 */
function thistheme_admin_title($admin_title, $title)
{
    return get_bloginfo('name').' &bull; ' . $title;
}
add_filter('admin_title', 'thistheme_admin_title', 10, 2);

function thistheme_login_title($origtitle) {
    return get_bloginfo('name');
}
add_filter('login_title', 'thistheme_login_title', 99);

function revealid_add_id_column( $columns ) {
   $columns['revealid_id'] = 'ID <style>.column-revealid_id { width: 100px !important;}</style>';
   return $columns;
}
add_filter( 'manage_posts_columns', 'revealid_add_id_column', 5 );

function revealid_id_column_content( $column, $id ) {
  if( 'revealid_id' == $column ) {
    echo $id;
  }
}
add_action( 'manage_posts_custom_column', 'revealid_id_column_content', 5, 2 );

function thistheme_manage_columns( $columns ) {
  unset($columns['comments']);
  return $columns;
}

function thistheme_column_init() {
  add_filter( 'manage_posts_columns' , 'thistheme_manage_columns' );
}
add_action( 'admin_init' , 'thistheme_column_init' );


require_once get_template_directory() . '/options.php';
