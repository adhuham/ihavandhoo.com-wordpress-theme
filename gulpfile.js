const { watch, src, dest, series, parallel } = require('gulp');
const tailwindcss = require('tailwindcss');

function tailwindCss() {
  const postcss = require('gulp-postcss')

  return src('resources/tailwind.css')
    .pipe(postcss([
      tailwindcss('./tailwind.config.js'),
      require('postcss-mixins'),
      require('autoprefixer'),
      require('postcss-nested')
    ]))
    .pipe(dest('css/'));
}

function appCss() {
  const postcss = require('gulp-postcss')

  return src('resources/app.css')
    .pipe(postcss([
      tailwindcss('./tailwind.config.js'),
      require('postcss-mixins'),
      require('autoprefixer'),
      require('postcss-nested')
    ]))
    .pipe(dest('css/'));
}

exports.css = parallel(tailwindCss, appCss);
