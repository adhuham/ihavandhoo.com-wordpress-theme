<!DOCTYPE html>
<html lang="dv" dir="rtl" translate="no">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">

   <?php thistheme_social_meta(); ?>
   <?php wp_head(); ?>
</head>

<body id="app" <?php body_class(); ?>>

  <header>
    <div class="header-space"></div>

    <?php $menus = wp_get_nav_menu_items( get_nav_menu_locations()['primary'] ?? null ); ?>

    <div class="main-nav fixed top-0 left-0 right-0 mt-0">
      <div class="container">

        <div class="row">
            <div class="col-8">
                <div class="flex">
                  <a href="<?php echo home_url(); ?>" class="logo">
                      <img src="<?php echo get_template_directory_uri() . '/assets/favicon.png'; ?>">
                  </a>
                  <nav class="hidden lg:flex lg:flex-row">
                    <?php foreach ($menus as $menu) : ?>
                        <a href="<?php echo $menu->url; ?>"><?php echo $menu->title; ?></a>
                    <?php endforeach; ?>
                  </nav>
                </div>
            </div>
            <div class="col-4 flex items-center justify-end relative">
                <div class="nav-side flex justify-end items-center">
                  <span class="ml-4 lg:ml-8 flex flex-col sm:flex-row items-center">
                    <div class="hidden lg:block">
                        <?php echo date('l, d M, Y'); ?>
                    </div>
                    <div class="hidden lg:flex lg:text-xl sm:mr-6">
                      <a href="https://facebook.com/ihavandhoo" class="hover:opacity-75 ml-4">
                        <i class="fab fa-facebook-f"></i>
                      </a>
                      <a href="https://twitter.com/ihavandhoo" class="hover:opacity-75 ml-4">
                        <i class="fab fa-twitter"></i>
                      </a>
                      <a href="https://youtube.com/ihavandhoo" class="hover:opacity-75">
                        <i class="fab fa-youtube"></i>
                      </a>
                    </div>
                  </span>
                  <a class="text-2xl cursor-pointer" data-menu-toggle>
                    <i class="fas fa-bars"></i>
                  </a>
                </div>

                <div class="dropdown absolute left-5 lg:left-0 z-40 hidden pb-1 select-none rounded-xl"
                  style="top: 100px;"
                  data-menu-dropdown>
                  <div class="flex flex-col py-2">
                    <?php foreach ($menus as $menu) : ?>
                     <a href="<?php echo $menu->url; ?>"><?php echo $menu->title; ?></a>
                    <?php endforeach; ?>
                  </div>
                </div>
          </div>
        </div>

      </div>
    </div>

  </header>


  <main>
