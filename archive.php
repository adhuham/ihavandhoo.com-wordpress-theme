<?php get_header(); ?>    

<?php
	if ( is_category() ) {
        $title = single_cat_title( '', false );
    }
    elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    }
    elseif ( is_author() ) {
        $title = get_the_author();
    }
    else {
    	$title = get_the_archive_title();
    }
?>

<div class="container mt-8 lg:mt-20">
    <div class="row">
        <div class="col-6">

          <div class="section-bar">
            <span class="section-title">
                <?php echo $title; ?>
            </span>
          </div>

          <?php  while ( have_posts() ) : the_post(); ?>
          <?php $post = get_post( get_the_ID() ); ?>

        <a href="<?php echo get_permalink( $post->ID ); ?>"
            class="post-card flex flex-row mt-4">
            <div class="w-2/6">
                <img src="<?php echo get_the_post_thumbnail_url( $post ); ?>">
            </div>
            <div class="w-4/6 mr-4 sm:ml-6">
              <time class="mt-0"><?php echo get_the_time('F d, Y', $post); ?></time>
              <h3 class="title"><?php echo get_post_meta($post->ID, 'custom-short-heading', true); ?></h3>
            </div>
          </a>

          <?php endwhile; ?>

            <div class="pagination-block mt-10 ltr">
                <?php echo paginate_links(array(
                    'prev_text' => '«',
                    'next_text' => '»'
                )); ?>
            </div>

        </div>
    </div>
</div>


<?php get_footer(); ?> 
