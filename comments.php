<div class="detail-comment mt-4">
    <div class="section-bar border-t-0 border-b-0 mb-0 pt-5">
      <a href="#comments" class="section-title hover:no-underline">
        ހިޔާލު ފާޅުކުރުން
      </a>
    </div>
    <div class="px-2 mb-8">
    <?php
        comment_form(
            array(
                'must_log_in' => '',
                'logged_in_as' => '',
                'comment_notes_before' => '',
                'title_reply' => 'ޚިޔާލު ފައުޅުކުރުން',
                'title_reply_to' => '%s އަށް ރިޕްލައި ކުރަށްވާ',
                'cancel_reply_link' => 'ކެންސަލް ކުރަށްވާ',
                'label_submit' => 'Send Comment',

                'comment_field' => '
                    <textarea
                      name="comment"
                      rows="5"
                      required
                      class="tkb font-faseyha border border-gray-400 p-4 rounded-xl w-full mt-4" rows="4"
                      placeholder="ކޮމެންޓް"></textarea>',

                'fields' => array(
                    'cookies' => ''
                )
            )
        );
     ?>
    </div>


    <a id="comments"></a>

    <?php
        $comments = get_comments(array(
            'post_id' => get_the_ID(),
            'status' => 'approve' //Change this to the type of comments to be displayed
        ));
        
        if ( !empty( $comments ) ): ?>
    <div class="comments">
      <?php foreach ($comments as $comment) : ?>
      <div class="pb-6 pl-6 pr-6 pt-4 mb-4 text-right bg-white rounded-xl"
            style="max-width: 600px">
        <div class="flex flex-row-reverse items-center justify-end">
          <time class="text-xs text-gray-600 font-medium">
            <?php echo get_comment_date ( 'M d, Y H:i', $comment->ID ); ?>
          </time>
        </div>
        <div class="font-faseyha text-gray-700 mt-4 sm:pl-12" style="font-size: 16px">
            <?php echo $comment->comment_content; ?>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
    <?php endif; ?>
</div>
