module.exports = {
  important: '#app',
  future: {},
  purge: [],
  theme: {
    extend: {
      colors: {
        'dotcom-green': '#006d46'
      }
    },
  },
  variants: {},
  plugins: [],
  corePlugins: {
     container: false
  }
}
