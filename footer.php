
  </main>

  <footer class="bg-white pt-8 pb-2 lg:pb-12 lg:pt-16 text-gray-700 mt-24">
    <div class="container mx-auto flex flex-col justify-between">
        <div class="row flex-col-reverse lg:flex-row">
            <div class="lg:col-6">
              <div class="border-t border-gray-300 pt-2 mt-2 text-xs 
                           text-center lg:text-right ltr lg:border-0 lg:pt-0 lg:mt-0 lg:text-sm">
                  <p class="mb-1 lg:mb-2">E-mail: <a href="mailto:contact@ihavandhoo.com">contact@ihavandhoo.com</a></p>
                  <p>Phone: <a href="tel:+9609951695">+960 9951695</a></p>
              </div>
            </div>
            <div class="lg:col-6 flex flex-col justify-center items-center lg:flex-row-reverse justify-start">
              <div class="logo mr-2 mb-3 lg:mb-0">
                  <img src="<?php echo get_template_directory_uri() . '/assets/favicon.png'; ?>" class="w-16 lg:w-20">
              </div>
              <div class="text-center lg:text-left ltr">
                  <p class="text-xs lg:text-base mb-0">Ihavandhoo.com</p>
                  <p class="text-xs mt-0 mb-8 lg:mb-2">&copy; <?php echo date('Y'); ?> All rights reserved.</p>
                  <p class="text-xs text-gray-500">
                    This is a registered online news magazine since 17th March 2011.
                  </p>
              </div>

            </div>
        </div>
    </div>
  </footer>

  <?php wp_footer(); ?>

</body>
</html>
