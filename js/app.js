jQuery(function ($) {

    $(document).ready(function() {
      $('.tkb').thaana();

      const headerElem = $('header');

      $(window).on('scroll', function() {
        let pos = window.scrollY;
        if (pos > 100) {
          headerElem.addClass('header-fixed');
        } else {
          headerElem.removeClass('header-fixed');
        }
      });

      $('[data-menu-toggle]').on('click', function() {
        $('[data-menu-dropdown]').toggleClass('hidden');
      });

      $('[data-share]').on('click', function(e) {
        e.preventDefault();

        let platform = $(this).data('share');
        let url = $(this).data('url');

        switch (platform) {
          case 'facebook':
            shareUrl = 'http://www.facebook.com/sharer.php?s=100&p[url]=' + url;
            break;

          case 'twitter':
            shareUrl = 'http://twitter.com/share?url=' + url;
            break;

          case 'viber':
            shareUrl = 'viber://forward?text=' + url;
            break;

          case 'whatsapp':
            shareUrl = 'whatsapp://send?text=' + url;
            break;

          case 'telegram':
            shareUrl = 'https://t.me/share/url?url=' + url;
            break;
        }

        let winTop = (screen.height / 2) - (600 / 2);
        let winLeft = (screen.width / 2) - (600 / 2);

        window.open(
          shareUrl,
          'sharer',
          'top=' + winTop + ',left=' + winLeft +
          ',toolbar=0,status=0,width=' + 600 + ',height=' + 600
        );
      });
    });
});
