<?php

function thistheme_get_option_by_slug ( $prefix, $slug ) {
    $options = [
        'featured' => 1,
        'small_1' => 2,
        'small_2' => 3,
        'thumb_1' => 4,
        'thumb_2' => 5,
        'thumb_3' => 6,
        'thumb_4' => 7,
        'thumb_5' => 8,
        'category_id_1' => 9,
        'category_id_2' => 10,
        'category_id_3' => 11,
    ];

	return get_option(THISTHEME_SLUG . '-' . $prefix . '-' . $options[$slug] );
}

function thistheme_get_option($option_name) {
	return get_option(THISTHEME_SLUG.'-'.$option_name);
}

// add settings page to menu 
function thistheme_options_create_menu() {
	add_theme_page(
		'Theme Options', // page title
		'Layout', // title in the page
		'manage_options',  // 
		THISTHEME_SLUG.'-options', // url/slug 
		'thistheme_options_page'  // functions that outputs option's html code
	);
}
add_action('admin_menu', 'thistheme_options_create_menu');


// setup sections and fields
function thistheme_options_fields() {
  
	add_settings_section(THISTHEME_SLUG.'-home-layout', 'Home Layout', null, 'home-layout-grp');
	
	$options = [
		1  => 'Featured',
		2  => 'Small 1',
		3  => 'Small 2',
		4  => 'Thumb 1',
		5  => 'Thumb 2',
		6  => 'Thumb 3',
		7  => 'Thumb 4',
		8  => 'Thumb 5',
		9  => 'Category ID 1',
		10 => 'Category ID 2',
		11 => 'Column Category ID'
	];
	foreach($options as $x => $opt) {
		add_settings_field(
			THISTHEME_SLUG.'-home-layout-'.$x, // option name
			$opt,
			function() use($x) {
				thistheme_field_home_layout($x);
			},
			'home-layout-grp', 
			THISTHEME_SLUG.'-home-layout'
		);

		register_setting(THISTHEME_SLUG.'-home-layout', THISTHEME_SLUG.'-home-layout-'.$x);
	}
}
add_action('admin_init', 'thistheme_options_fields');


function thistheme_field_home_layout($pos) {

	$option_val =  get_option(THISTHEME_SLUG.'-home-layout-'.$pos);
?>
	<input type="text" name="<?php echo THISTHEME_SLUG.'-home-layout-'.$pos; ?>" value="<?php echo $option_val; ?>">
<?php

}


function thistheme_field_home_cat($pos) {

	$option_val =  get_option(THISTHEME_SLUG.'-home-cat-'.$pos);
?>
	<select name="<?php echo THISTHEME_SLUG.'-home-cat-'.$pos; ?>">
		<option value="0">None</option>
		<?php foreach(get_categories() as $cat): ?>
			<option value="<?php echo $cat->term_id; ?>"<?php echo ($cat->term_id == $option_val) ? ' selected' : ''; ?>>
				<?php echo $cat->name; ?>
			</option>
		<?php endforeach; ?>
	</select>
<?php
	

}

function thistheme_options_page() {
    ?>
	    <div class="wrap">
	    <h1><?php echo THISTHEME_NAME; ?> Options</h1>
	    <hr><br>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields(THISTHEME_SLUG.'-home-layout');
	            do_settings_sections('home-layout-grp');      
	            submit_button(); 
	        ?>          
	    </form>
	    </div>
	<?php
}

