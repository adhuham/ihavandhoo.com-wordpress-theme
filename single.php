<?php get_header(); ?>    

<?php $post = get_post( get_the_ID() ); ?>
<div class="container mt-10 lg:mt-20">
    <div class="row">
        <div class="lg:col-8">

          <div class="detail-title text-right">
            <time class="block font-medium text-gray-700 text-lg mb-4">
                <?php echo get_the_time('F d, Y H:i', $post); ?>
            </time>
            <div class="text-right pb-2">
              <h1 class="font-aaamu-fk mt-2 pb-0 mb-0 text-4xl">
                <?php echo $post->post_title; ?>
              </h1>
            </div>
            <span class="font-waheed block mt-2" style="font-size: 19px">
                <?php echo get_the_author_meta('display_name', $post->post_author); ?>
            </span>
          </div>

          <div class="detail-featured mt-8">
            <figure>
              <img src="<?php echo get_the_post_thumbnail_url( $post ); ?>" class="w-full rounded-xl">
              <?php $caption = get_the_post_thumbnail_caption ( $post ); ?>
              <?php if (!empty( $caption )) : ?>
              <figcaption class="font-faseyha text-right border-b border-gray-200
                                 py-3 px-2 block text-gray-500">
                <?php echo $caption; ?>
              </figcaption>
              <?php endif; ?>
            </figure>
          </div>

          <div class="detail-content pt-0 pb-4 mt-8 flex flex-row-reverse flex-wrap">
            <div class="w-full sm:w-5/6 px-4 sm:pl-12 sm:pr-12 sm:border-r ms:border-gray-300 text-right">

              <div class="content mt-6">
                <?php $content = apply_filters( 'the_content', get_the_content() ); ?>
                <?php echo $content; ?>
              </div>

              <div class="tags mb-4 pt-4 text-right">
                <?php $tags = get_the_tags( $post ); ?>

                <?php if (!empty( $tags )) : ?>
                <?php foreach( $tags as $tag ) : ?>
                <a href="<?php echo get_tag_link( $tag ); ?>"
                  class="font-waheed px-4 py-2 bg-white text-gray-600 rounded-xl
                         inline-block ml-2 hover:bg-gray-300 hover:no-underline mb-2">
                  <?php echo $tag->name; ?>
                </a>
                <?php endforeach; ?>
                <?php endif; ?>
              </div>

            </div>
            <div class="w-full sm:w-1/6 pl-4 sm:pl-8 pt-3 sm:pt-8 sm:pr-5 text-right">

              <?php $share_url = get_permalink ( $post ); ?>
              <div class="detail-content-share space-x-3 pr-4 sm:pr-0 flex-row sm:flex-col sm:space-x-0">
                <a href="#" data-url="<?php echo $share_url; ?>" data-share="facebook">
                  <i class="fab fa-facebook-f"></i>
                </a>
                <a href="#" data-url="<?php echo $share_url; ?>" data-share="twitter">
                  <i class="fab fa-twitter"></i>
                </a>
                <a href="#" data-url="<?php echo $share_url; ?>" data-share="telegram">
                  <i class="fab fa-telegram-plane"></i>
                </a>
                <a href="#" data-url="<?php echo $share_url; ?>" data-share="whatsapp">
                  <i class="fab fa-whatsapp"></i>
                </a>
                <a href="#" data-url="<?php echo $share_url; ?>" data-share="viber">
                  <i class="fab fa-viber"></i>
                </a>
              </div>

            </div>

          </div>

           <?php get_template_part ('comments'); ?>

        </div>
        <div class="lg:col-4">
          <div class="section-bar mt-4">
            <span class="section-title">
              ފަހުގެ
            </span>
          </div>

          <?php
              $posts = get_posts(['exclude' => [$post->ID], 'orderby' => 'date', 'order' => 'DESC', 'numberposts' => 6]);
          ?>
        
          <?php foreach ($posts as $post) : ?> 
          <a href="<?php echo get_permalink( $post ); ?>"
                 class="post-card flex flex-row mb-4">
            <div class="w-5/12">
                <img src="<?php echo get_the_post_thumbnail_url( $post, 'thistheme-thumb' ); ?>">
            </div>
            <div class="w-8/12 mr-4 ml-6">
              <time class="mt-0"><?php echo get_the_time('F d, Y', $post); ?></time>
              <h4 class="title">
                <?php echo get_post_meta($post->ID, 'custom-short-heading', true); ?>
              </h4>
              <p class="excerpt mt-1 text-gray-500 text-xs"></p>
            </div>
          </a>
          <?php endforeach; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?> 
