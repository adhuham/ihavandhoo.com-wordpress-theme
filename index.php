<?php get_header(); ?>    

<?php $exclude_ids = []; ?>

<div class="container mt-8 lg:mt-20 mb-10">
    <div class="row justify-center">
        <div class="lg:col-7">

            <?php $featured = get_post( thistheme_get_option_by_slug('home-layout', 'featured') ); ?>
            <?php $exclude_ids[] = $featured->ID; ?>

             <a href="<?php echo get_permalink( $featured ); ?>" class="post-card pb-5 hover:text-dotcom-green text-black mb-8 lg:mb-0">
                <img src="<?php echo get_the_post_thumbnail_url( $featured ); ?>" class="w-full rounded-xl">
                <time class="text-sm lg:text-base mb-3 mt-4 lg:mt-6 lg:text-center font-medium">
                    <?php echo get_the_time('F d, Y', $featured); ?>
                </time>
                <h1 class="title mt-1 lg:text-center px-2">
                  <?php echo get_post_meta($featured->ID, 'custom-short-heading', true); ?>
                </h1>
                <p class="excerpt text-base text-center mx-auto" style="max-width: 550px">
                  <?php // echo get_the_excerpt( $featured ); ?>
                </p>
              </a>

        </div>
        <div class="lg:col-4">

            <?php for ( $i = 1; $i <= 4; $i++ ): ?>
            <?php $post = get_post( thistheme_get_option_by_slug( 'home-layout', 'thumb_' . $i ) ); ?>
            <?php $exclude_ids[] = $post->ID; ?>

              <a href="<?php echo get_permalink ( $post ); ?>" class="post-card flex mb-4">
                <div class="w-5/12">
                    <img src="<?php echo get_the_post_thumbnail_url( $post, 'thistheme-thumb' ); ?>" class="rounded-xl">
                </div>
                <div class="w-7/12 mr-4 sm:ml-6">
                  <time class="mt-0"><?php echo get_the_time('F d, Y', $post); ?></time>
                  <h3 class="title">
                    <?php echo get_post_meta($post->ID, 'custom-short-heading', true); ?>
                  </h3>
                  <p class="excerpt mt-1 text-gray-500 text-xs">
                     <?php // echo get_the_excerpt( $post ); ?>
                  </p>
                </div>
              </a>

            <?php endfor; ?>

        </div>
        <div class="lg:col-2"></div>
    </div>
</div>


<?php
    $option = thistheme_get_option_by_slug('home-layout', 'category_id_1');
    $category = get_category( $option );
    $posts = get_posts(['category' => $option, 'orderby' => 'date', 'order' => 'DESC', 'numberposts' => 10]);
?>

<div class="container">

    <div class="row">
        <div class="col-12">
          <div class="section-bar">
            <span class="section-title">
                <?php echo $category->name; ?>
            </span>
          </div>
        </div>
    </div>

  <div class="row">

    <div class="lg:col-5 mb-10 lg:mb-0">

      <?php $exclude_ids[] = $posts[0]->ID; ?>
      <a href="<?php echo get_permalink( $posts[0] ); ?>" 
        class="post-card w-full h-full relative rounded-xl overflow-hidden">
        <img
          class="sm:absolute w-full h-full object-cover"
          style="min-height: 400px"
          src="<?php echo get_the_post_thumbnail_url( $posts[0] ); ?>">
        <div class="absolute bottom-0 top-0 left-0 right-0"
          style="background: linear-gradient(1.63deg, rgba(0, 0, 0, 0.74) 24.12%, rgba(0, 0, 0, 0) 98.55%);">
          <div class="absolute bottom-0 left-0 right-0 px-8 pb-6 sm:px-12 sm:pb-12">
            <time class="text-white"><?php echo get_the_time('F d, Y', $posts[0]); ?></time>
            <h2 class="title text-white hover:text-white lg:text-4xl">
              <?php echo get_post_meta($posts[0]->ID, 'custom-short-heading', true); ?>
            </h2>
            <p class="excerpt text-gray-300 text-sm">
             <?php echo get_the_excerpt( $posts[0] ); ?>
            </p>
        </div>
        </div>
      </a>

    </div>

    <div class="lg:col-4">
       <?php $exclude_ids[] = $posts[1]->ID; ?>
      <a href="<?php echo get_permalink( $posts[1] ); ?>" class="post-card mb-8">
        <img src="<?php echo get_the_post_thumbnail_url( $posts[1], 'thistheme-thumb' ); ?>" class="w-full rounded-xl">
        <time><?php echo get_the_time('F d, Y', $posts[1]); ?></time>
        <h2 class="title">
            <?php echo get_post_meta($posts[1]->ID, 'custom-short-heading', true); ?>
        </h2>
      </a>

     <?php foreach ([$posts[2], $posts[3]] as $post) : ?>
     <?php $exclude_ids[] = $post->ID; ?>
      <a href="<?php echo get_permalink( $post ); ?>" class="post-card flex mt-4">
        <div class="w-2/6">
          <img src="<?php echo get_the_post_thumbnail_url( $post, 'thistheme-thumb' ); ?>">
        </div>
        <div class="w-4/6 mr-4">
          <time class="mt-0"><?php echo get_the_time('F d, Y', $post); ?></time>
          <h4 class="title">
            <?php echo get_post_meta($post->ID, 'custom-short-heading', true); ?>
          </h4>
        </div>
      </a>
      <?php endforeach; ?>
    </div>
    

    <div class="lg:col-6">
    </div>
  </div>
</div>


<?php
    $option = thistheme_get_option_by_slug('home-layout', 'category_id_2');
    $category = get_category( $option );
    $posts = get_posts(['category' => $option, 'orderby' => 'date', 'order' => 'DESC', 'numberposts' => 4]);
?>

<div class="container mt-20">

    <div class="row">
        <div class="col-12">
          <div class="section-bar">
            <span class="section-title">
                <?php echo $category->name; ?>
            </span>
          </div>
        </div>
    </div>

  <div class="row">

    <?php for ( $i = 0; $i <= 3; $i++ ): ?>
    <?php $post = $posts[$i]; ?>
    <?php $exclude_ids[] = $post->ID; ?>

    <div class="lg:col-3">
      <a href="<?php echo get_permalink( $post ); ?>" class="post-card ml-4 mb-8 lg:mb-0">
        <img src="<?php echo get_the_post_thumbnail_url( $post, 'thistheme-thumb' ); ?>" class="rounded-xl">
        <time><?php echo get_the_time('F d, Y', $post); ?></time>
        <h3 class="title">
            <?php echo get_post_meta($post->ID, 'custom-short-heading', true); ?>
        </h3>
      </a>
    </div>
    <?php endfor; ?>

  </div>
</div>


<div class="container mt-20">
    <div class="row">

        <div class="lg:col-3">
            <div class="hidden lg:block w-full">
              <div class="w-full bg-gray-300 rounded-xl ml-4" style="height: 400px"></div>
            </div>
        </div>

        <div class="lg:col-6">
          <div class="section-bar">
            <span class="section-title">
              އިތުރު ޚަބަރު
            </span>
          </div>

          <?php
              $posts = get_posts(['exclude' => $exclude_ids, 'orderby' => 'date', 'order' => 'DESC', 'numberposts' => 10]);
          ?>

          <?php for ( $i = 1; $i <= 10; $i++ ): ?>
          <?php $post = $posts[$i]; ?>

          <a href="<?php echo get_permalink( $post ); ?>"
            class="post-card flex flex-row mt-4">
            <div class="w-2/6">
                <img src="<?php echo get_the_post_thumbnail_url( $post, 'thistheme-thumb' ); ?>" class="rounded-xl">
            </div>
            <div class="w-4/6 mr-4 sm:ml-6">
              <time class="mt-0"><?php echo get_the_time('F d, Y', $post); ?></time>
              <h3 class="title">
                <?php echo get_post_meta($post->ID, 'custom-short-heading', true); ?>
              </h3>
              <p class="excerpt mt-1 text-gray-500 text-xs">
                 <?php // echo get_the_excerpt( $post ); ?>
              </p>
            </div>
          </a>

          <?php endfor; ?>
        </div>

        <?php /*
        <div class="lg:col-3">
          <?php
              $option = thistheme_get_option_by_slug('home-layout', 'category_id_3');
              $category = get_category( $option );
              $posts = get_posts(['category' => $option, 'orderby' => 'date', 'order' => 'DESC', 'numberposts' => 10]);
          ?>
          <div class="section-bar">
            <span class="section-title">
                <?php echo $category->name; ?>
            </span>
          </div>

          <?php for ( $i = 1; $i <= 4; $i++ ): ?>
          <?php $post = $posts[$i]; ?>

          <a href="<?php echo get_permalink( $post ); ?>"
              class="post-card flex flex-row border-b border-gray-200 pb-4 mt-4">
            <img class="rounded-full w-16 h-16" src="<?php echo get_the_post_thumbnail_url( $post ); ?>">
            <div class="mr-4">
              <h4 class="title">
                <?php $kicker = get_post_meta($post->ID, 'custom-kicker', true); ?>
                <?php if (!empty( $kicker )) : ?>
                   <span class="title-kicker pl-6 mt-0"><?php echo $kicker; ?></span>
                <?php endif; ?>
                <?php echo get_post_meta($post->ID, 'custom-short-heading', true); ?>
              </h4>
            </div>
          </a>

          <?php endfor; ?>
        </div>
        */ ?>

    </div>
    
  </div>
</div>

<?php get_footer(); ?> 
